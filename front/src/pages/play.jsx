import React from "react";
import { connect } from "react-redux";
import Private from "layouts/private";
import CardChoose from "components/CardChoose";
import CardGame from "components/CardGame";


const Play = ({cards, dispatch}) => {
    const [isGameOk, setIsGameOk] = React.useState(false)
    const test= (selectedCardId)=>{
        console.log(selectedCardId)
        dispatch({
            type: 'SET_SELECTED_GAME_CARD_ID',
            selectedCardId: selectedCardId
        });
        setIsGameOk(true)
    }
    return (
        
        <div>
                <Private namePage="Jouer">
                    {!isGameOk ? (<CardChoose cards={cards} handlePlayClick={test}></CardChoose>) : null}
                    {isGameOk ? (<CardGame></CardGame>) : null}

                </Private>

            </div>
    );
}
export default connect(state => ({
    cards: state.card.cards.filter(card => card.isOwned == true),
}))(Play);
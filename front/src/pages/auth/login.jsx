import React from "react";
import Auth from "layouts/auth";
import Connexion from "components/Connexion";


export default function login() {
    return (
            <Auth>
                <Connexion></Connexion>
            </Auth>
    );
}
import React from "react";
import Private from "layouts/private";
import CardsList from "components/CardsList";
import { connect } from "react-redux";
import Card from "components/Card";

const Buy = ({cards, dispatch, selectedCard}) => {
    return (
        <Private namePage="Acheter">
            <div className="flex">
                <CardsList cards={cards} actionButton="Acheter" className="flex-grow">
                </CardsList>
                {selectedCard ?
                    <div className="flex flex-shrink p-4 pr-0">
                        <Card card={selectedCard}></Card>
                    </div>
                    : null
                }
            </div>
        </Private>
    );

}

export default connect(state => ({
    cards: state.card.cards.filter(card => card.userId == 0),
        selectedCard : state.card.cards.find(x => x?.id === state.card.selectedCardId)
}))(Buy);
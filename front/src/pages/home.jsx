import React from "react";
import MenuButton from "components/MenuButton";
import Private from "layouts/private";

export default function home() {
    return (

        
        <div>
                <Private namePage="Accueil">
                <div className="p-8 ...">
                <ul role="list" className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3">
                        <MenuButton to="/acheter" name="Acheter"></MenuButton>
                        <MenuButton to="/vendre" name="Vendre"></MenuButton>
                        <MenuButton to="/jouer" name="Jouer"></MenuButton>
                </ul>
                </div>
                </Private>

            </div>
    );
}
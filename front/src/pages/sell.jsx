import React, { useEffect } from "react";
import Private from "layouts/private";
import CardsList from "components/CardsList";
import { connect } from "react-redux";
import Card from "components/Card";
import { useDispatch } from "react-redux";
import { SET_CARDS } from "store/actions";

const Sell = ({cards, selectedCard}) => {
    const dispatch = useDispatch();


    useEffect(function (){
        fetch('http://127.0.0.1:8091/cards')
        .then(response => response.json())
        .then(data => {
            console.log(data);
            dispatch({ type: SET_CARDS, cards: data });
        })
    },[])

    //useSelector

    

    return (
        <Private namePage="Vendre">
            <div className="flex">
                <CardsList cards={cards} actionButton="Vendre" className="flex-grow">
                </CardsList>
                {selectedCard ?
                    <div className="flex flex-shrink p-4 pr-0">
                        <Card card={selectedCard}></Card>
                    </div>
                    : null
                }
            </div>
        </Private>
    );
}

export default connect(state => ({
    cards: state.card.cards.filter(x =>{ return x.userId != 0}),
    selectedCard : state.card.cards.find(x => x?.id === state.card.selectedCardId)
}))(Sell);


import React from 'react';
import { Route, Navigate } from 'react-router-dom';
// import { isLogin } from '../utils';

const isLogin = () => false;

const PrivateRoute = ({element: Element, children, ...rest}) => {
    return (

        // Show the component only when the user is logged in
        // Otherwise, redirect the user to /signin page
        <Route {...rest} render={props => (
            isLogin() ?
                {children}
            : <Navigate to="/signin" />
        )} />
    );
};

export default PrivateRoute;
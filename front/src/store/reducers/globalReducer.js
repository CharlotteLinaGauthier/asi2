import { combineReducers } from 'redux';
import card from "./card";
import game from "./game";
import user from "./user";

// combine reducers

const globalReducer = combineReducers({
    card,
    game,
    user
});

export default globalReducer;
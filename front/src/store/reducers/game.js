import { SET_SELECTED_GAME_CARD_ID } from '../actions'

const initialState = {
  selectedCardId: null,
  /*...*/
}

/**
 * Reducer
 */
const gameReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SELECTED_GAME_CARD_ID:
        console.log(action);
        return {
            ...state,
            selectedCardId: action.selectedCardId,
      }
    default:
      return state
  }
}

export default gameReducer
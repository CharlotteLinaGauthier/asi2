import { SET_CARDS, SET_SELECTED_CARD_ID, UPDATE_CARD } from '../actions'

const initialState = {
  cards: []/*[
  {
    id: 1,
    name: "Goupix",
    description: "desc",
    family: "family",
    hp: 50,
    energy: 100,
    defence: 80,
    attack: 120,
    price: 200,
    img: "https://www.pokepedia.fr/images/thumb/a/ab/Goupix-EdC.png/200px-Goupix-EdC.png",
    isOwned: false,
  },
  {
      id: 2,
      name: "Togepi",
      description: "desc",
      family: "family",
      hp: 50,
      energy: 100,
      defence: 80,
      attack: 120,
      price: 200,
      img: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/175.png",
      isOwned: false,
  },
  {
    id: 3,
    name: "Pikachu",
    description: "desc",
    family: "family",
    hp: 50,
    energy: 100,
    defence: 80,
    attack: 120,
    price: 200,
    img: "https://www.cdiscount.com/pdt2/4/7/9/1/700x700/roo0034878149479/rw/sticker-geant-repositionnable-pikachu-pokemon-nint.jpg",
    isOwned: false,

  },
  {
      id: 4,
      name: "Rondoudou",
      description: "desc",
      family: "family",
      hp: 50,
      energy: 100,
      defence: 80,
      attack: 120,
      price: 200,
      img: "https://www.pokepedia.fr/images/thumb/c/cd/Rondoudou-RFVF.png/250px-Rondoudou-RFVF.png",
      isOwned: true,

  },
  {
    id: 5,
    name: "Salameche",
    description: "desc",
    family: "family",
    hp: 50,
    energy: 100,
    defence: 80,
    attack: 120,
    price: 200,
    img: "https://www.pokebip.com/pokedex-images/300/4.png",
    isOwned: true,
},]*/
,
  selectedCardId: null,
  /*...*/
}

/**
 * Reducer
 */
const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CARDS:
      // state is immutable, you must create a new object with another reference
        return {
            ...state,
            cards: action.cards,
        }
    case SET_SELECTED_CARD_ID:
        console.log(action);
        return {
            ...state,
            selectedCardId: action.cardId,
      }
    case UPDATE_CARD:
      console.log(action.cardId);
      const index=state.cards.findIndex(x=> x.id === action.card.id);
      state.cards[index] = action.card;
      return {
          ...state,
          cards: state.cards,
      }
    default:
      return state
  }
}

export default cardReducer
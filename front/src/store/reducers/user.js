import { SET_USER } from '../actions'

const initialState = {
    loggedInUser: {
        firstName: 'hugo',
        lastName: 'venancio',
        balance: 999999,
        profilePicture: "https://www.leparisien.fr/resizer/tOJc-dzVnGERoMe-tHl_3UFnXd4=/932x582/cloudfront-eu-central-1.images.arcpublishing.com/leparisien/WBFPQ2OHDFEZLIZEZ2SBQBHAHA.jpg"
    },
}

/**
 * Reducer
 */
const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
        return {
            ...state,
            loggedInUser: action.user,
        }
    default:
      return state
  }
}

export default cardReducer
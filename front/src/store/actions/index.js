/*
 * action types
 */

export const SET_SELECTED_CARD_ID = 'SET_SELECTED_CARD_ID';
export const SET_CARDS = 'SET_CARDS';
export const UPDATE_CARD = 'UPDATE_CARD';
export const SET_SELECTED_GAME_CARD_ID= 'SET_SELECTED_GAME_CARD_ID';
export const SET_USER = 'SET_USER';

/*
 * action creators
 */

export function setCards(cards) {
  return { type: SET_CARDS, cards }
}

export function setSelectedCardId(cardId) {
  return { type: SET_SELECTED_CARD_ID, cardId }
}

export function updateCard(card) {
  return { type: UPDATE_CARD, card }
}

export function setSelectedGameCardId(selectedCardId){
  return { type:SET_SELECTED_GAME_CARD_ID, selectedCardId }
}
export function setUser(user) {
  return { type: SET_USER, user }
}
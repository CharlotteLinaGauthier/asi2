import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

// routes
import Signin from "pages/auth/signin";
import Login from "pages/auth/login";
import Home from "pages/home";
import Buy from "pages/buy";
import Sell from "pages/sell";
import Play from "pages/play";

// store
import { createStore } from "redux"
import globalReducer from "store/reducers/globalReducer";
import { Provider } from 'react-redux';
import PrivateRoute from "routes/PrivateRoute";

const store = createStore(
  globalReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
            <Route exact path="/" element={<Home/>}/>
            {/* <Route exact path="/" element={<PrivateRoute>
               <Home />
             </PrivateRoute>}/> */}
            <Route path="/login" element={<Login/>}/>
            <Route path="/signin" element={<Signin/>}/> 
            <Route path="/acheter" element={<Buy/>}/>
            <Route path="/vendre" element={<Sell/>}/>
            <Route path="/jouer" element={<Play/>}/>

        </Routes>
      
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// function App() {
//   return (
//     <BrowserRouter>
//       <Routes>
//         <Route path="/" element={<Public />} />
//         <Route
//           path="/private"
//           element={
//             <PrivateRoute>
//               <Private />
//             </PrivateRoute>
//           }
//         />
//       </Routes>
//     </BrowserRouter>
//   );
// }
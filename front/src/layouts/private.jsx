import React from 'react';
import Header from 'components/Header';
class Private extends React.Component {
    render() {
        return (
            <div className="h-screen bg-blue-300">
                <div className="bg-blue-300">
                    <Header namePage={this.props.namePage}/>
                    <main className="-mt-28">
                        <div className="max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:px-8">
                            <div className="bg-white rounded-lg shadow px-5 py-6 sm:px-6">
                                {this.props.children}
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        )
    } 
}

export default Private;
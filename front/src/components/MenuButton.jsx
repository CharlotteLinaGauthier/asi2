import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class MenuButton extends Component {
    render() {
        return (
            
            <Link to={this.props.to}>
                 <li className="col-span-1 bg-white rounded-lg shadow divide-y divide-gray-200 ">
                        <div className="w-full flex items-center justify-between p-6 ">
                            <div className="flex-1 truncate text-center">
                                        {this.props.name}
                            </div>
                        </div>
                    </li>
            </Link>
        );
    }
}

export default MenuButton;
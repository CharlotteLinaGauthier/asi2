import CardListItem from "components/CardListItem";
import { connect } from "react-redux";

const CardsList = ({actionButton, cards, selectedCardId, className}) => {
    return (

        <div className={"flex flex-col " + className}>
            <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                {/* ${selectedCardId ? '' : 'min-w-full'} */}
                <div className={`py-2 align-middle inline-block sm:px-6 lg:px-8 transition-all w-full`}>
                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table className={`divide-y transition-all divide-gray-200 w-full`}>
                            <thead className="bg-gray-50">
                                <tr>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Description</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Family</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">HP</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Energy</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Defence</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Attack</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Price</th>
                                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"></th>
                                </tr>
                            </thead>
                            <tbody className="bg-white divide-y divide-gray-200">
                                {cards.map(card => (<CardListItem card={card} actionButton={actionButton} key={card.id}/>))}
                            </tbody>
                        </table>
                    </div>
            
                </div>
            </div>
        </div>
    );
}


export default connect(state => ({
    selectedCardId: state.card.selectedCardId
}))(CardsList);
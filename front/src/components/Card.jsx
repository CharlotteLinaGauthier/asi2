import { LightningBoltIcon, ShieldExclamationIcon } from "@heroicons/react/solid";

const Card = ({card}) => {    // name, desc, family,  price
    if(!card)
        return null;
    return (
        <div className="p-2 border rounded-md shadow">
            <div className="flex items-center">
                <div className="flex items-center justify-center rounded-full bg-gray-600 text-white h-4 w-4">
                    <span className="text-xs">{card.id}</span>
                </div>
                <div className="flex flex-grow font-bold justify-center">{card.name}</div>
            </div>
            <img 
                src={card.img}
                alt=""
                className="h-72 w-72 inline-block"
            />
            <div className="text-center text-xs text-gray-600">
                {card.description}
            </div>
            <div className="grid grid-cols-2 pt-2">
                <div className="flex justify-center">
                    <div className="flex h-7 w-7 bg-yellow-300 rounded-full text-white items-center justify-center shadow-md">
                        <LightningBoltIcon className="w-5 h-5"></LightningBoltIcon> 
                    </div>
                    <div>{card.defence}</div>
                </div>
                <div className="flex justify-center">
                    <div className="flex h-7 w-7 bg-blue-500 rounded-full text-white items-center justify-center shadow-md">
                        <ShieldExclamationIcon className="w-5 h-5"></ShieldExclamationIcon> 
                    </div>
                    <div>{card.attack}</div>
                </div>
            </div>
            <div className="grid grid-cols-2">
            </div>
        </div>
    );
}

export default Card;
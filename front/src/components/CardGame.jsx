import { connect } from "react-redux";
import Card from "./Card";

const CardGame = ({selctedGameCards}) => {  
    return (
    <>
    <p>JEU DE CARTE</p>
    <div className="grid grid-cols-4 gap-2">
     {(selctedGameCards.map(card => <Card card={card}/>))}
    </div>
    </>

    )
}
export default connect(state => ({
  selctedGameCards: state.card.cards.filter(x => state.game.selectedCardId.includes(x.id))
}))(CardGame);

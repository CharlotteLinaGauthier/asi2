import React, { Component } from 'react';
import { connect } from "react-redux";
import { CurrencyDollarIcon, ShoppingCartIcon } from '@heroicons/react/solid'


function sellOrBuyCard(e, actionButton,card,dispatch) {
    e.stopPropagation();
    if (actionButton == "Acheter"){
        //TODO Change le montant => Enlever prix de la carte au montant de l'user
        card.isOwned=true
        dispatch({
            type: 'UPDATE_CARD',
            card: card
        });
        //TODO : Appel API Update CARD
        axios.post('http://127.0.0.1:8092/buy', {login:this.state.username,pwd:this.state.pwd,email:this.state.email})
        .then(response => this.setState({ articleId: response.data.id }));
    } 
    else {
        //TODO Changer le montant => Rajouter prix de la carte au montant de l'user
        card.isOwned=false
        dispatch({
            type: 'UPDATE_CARD',
            card: card
        });
        //TODO : Appel API Update Card
    }
    }

const handleCardClick = (card, dispatch) => {
    dispatch({
        type: 'SET_SELECTED_CARD_ID',
        cardId: card.id
    });
}


const CardListItem = ({card, dispatch, actionButton, selectedCardId}) => {
    console.log(card);
    return (     
        <tr key={card.id} onClick={() => handleCardClick(card, dispatch)} className={selectedCardId === card.id ? 'bg-blue-100' : ''}>
            <td className="px-6 py-4 whitespace-nowrap">
                <div className="flex-shrink-0 h-10 w-10">
                    <img  className="h-10 w-10 rounded-full" src={card.imgUrl} alt=""/>
                </div>
                <div>{card.name}</div>
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                {card.description}
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                {card.family}
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                {card.hp}
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                {card.energy}
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                {card.defence}
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                {card.attack}
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                {card.price}
            </td>
            <td className="px-6 py-4 whitespace-nowrap">
                <button className="flex h-8 w-8 items-center justify-center bg-blue-300 rounded-full shadow-md" onClick={(e) => sellOrBuyCard(e, actionButton,card,dispatch)}>
                    {actionButton === "Acheter" ? 
                        <ShoppingCartIcon className="h-5 w-5 text-white" /> :
                        <CurrencyDollarIcon className="h-5 w-5 text-white" />
                    }
                </button>
            </td>
        </tr>
    )
}


export default connect(state => ({
    cards: state.card.cards,
    selectedCardId: state.card.selectedCardId
}))(CardListItem);
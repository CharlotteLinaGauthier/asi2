import Card from "components/Card";
import { useState } from "react";

// let selectedCardId=[];

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }

function selectCard(card, selectedCardId, setSelectedCardId) {
    console.log(selectedCardId, selectedCardId.includes(card.id))
    
    if (!selectedCardId.includes(card.id)){
        setSelectedCardId([...selectedCardId, card.id])
    }
    else{
        setSelectedCardId(selectedCardId.filter(x => x != card.id))
    }    
}
// function nextStep(handlePlayClick){
//     //TT VALIDER
//     handlePlayClick
// }

const CardChoose = ({cards,handlePlayClick}) => {  
    const [selectedCardId, setSelectedCardId] = useState([])
    return (
        <>
                            <p>Choissisez vos cartes</p>

<ul role="list" className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
      {cards.map((card) => (
        <li
          key={card.id}
          className="col-span-1 flex flex-col text-center bg-white rounded-lg shadow divide-y divide-gray-200"
        >
         <div  onClick={()=> selectCard(card, selectedCardId, setSelectedCardId)} className={classNames(
                                selectedCardId.includes(card.id)
                                  ? 'bg-white border-4 border-blue-300'
                                  : 'bg-white',
                                 'bg-white'
                              )} >
            <Card card={card} ></Card> 

         </div>
        </li>
      ))}
    </ul>
    <div className="flex justify-center">
    <button type="button"
        className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-gray-800 hover:bg-blue-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 mt-10"
        onClick={()=>handlePlayClick(selectedCardId)}>
            Suivant</button>
        </div>
    </>

    )
}
export default CardChoose;
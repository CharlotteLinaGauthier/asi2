package com.example.user.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.example.common.Client.CardClient;
import com.example.common.DTO.CardDTO;
import com.example.common.DTO.UserDTO;
import com.example.user.Model.UserModel;
import com.example.user.Repository.UserRepository;
import org.apache.catalina.mbeans.UserMBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CardClient cardModelService;

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		return userRepository.findById(id);
	}

	public void addUser(UserModel user) {
		// needed to avoid detached entity passed to persist error
		System.out.println(user.getLogin());
		userRepository.save(user);
		List<CardDTO> cardList=cardModelService.getRandCard(user.getId(), 5);
		for(CardDTO card: cardList) {
			user.addCard(card.getId());
		}
		userRepository.save(user);
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Integer.valueOf(id));
	}

	public List<UserModel> getUserByLoginPwd(String login, String pwd) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}



	public void addCard(Integer idCard, Integer idUser){
		UserModel user = this.getUser(idUser).orElseThrow();
		Set<Integer> userList = user.getCardList();
		CardDTO c = cardModelService.getcard(idCard);
		if(canBuy(idUser.toString(),String.valueOf(c.getPrice()))){
			userList.add(idCard);
			user.setCardList(userList);
			user.setAccount(user.getAccount()-c.getPrice());
			updateUser(user);
		}

	}

	public void removeCard(Integer idCard, Integer idUser){
		UserModel user = this.getUser(idUser).orElseThrow();
		Set<Integer> userList = user.getCardList();
		userList.removeIf(integer -> integer == idCard );
		CardDTO c = cardModelService.getcard(idCard);
		user.setAccount(user.getAccount()+c.getPrice());
		user.setCardList(userList);
		updateUser(user);
	}

	public boolean canBuy(String id, String price) {
		Optional<UserModel> user = getUser(id);
		return(user.get().getAccount()-Float.parseFloat(price)>=0);

	}

	public boolean connectUser(String login, String password) {
		return (this.getUserByLoginPwd(login, password)!=null);
	}
}

package com.example.user.Repository;

import java.util.List;

import com.example.user.Model.UserModel;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserModel, Integer> {
	List<UserModel> findByLoginAndPwd(String login,String pwd);
}

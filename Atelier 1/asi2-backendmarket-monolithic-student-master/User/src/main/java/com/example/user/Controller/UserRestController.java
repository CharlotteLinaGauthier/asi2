package com.example.user.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.user.Model.UserModel;
import com.example.user.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {

	@Autowired
	private UserService userService;

	@RequestMapping("/users")
	private List<UserModel> getAllUsers() {
		return userService.getAllUsers();

	}

	@RequestMapping("/user/{id}")
	private UserModel getUser(@PathVariable String id) {
		Optional<UserModel> ruser;
		ruser= userService.getUser(id);
		if(ruser.isPresent()) {
			return ruser.get();
		}
		return null;

	}

	@RequestMapping(method= RequestMethod.POST,value="/user")
	public void addUser(@RequestBody UserModel user) {
		userService.addUser(user);
	}

	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}")
	public void updateUser(@RequestBody UserModel user,@PathVariable String id) {
			user.setId(Integer.valueOf(id));
		userService.updateUser(user);
	}

	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public void deleteUser(@PathVariable String id) {
		userService.deleteUser(id);
	}



	@RequestMapping(method=RequestMethod.POST,value="/user/addCard")
	private void addCard(@RequestParam("userId") String userId, @RequestParam("cardId") String cardId) {
		userService.addCard(Integer.parseInt(userId),Integer.parseInt(cardId));
	}

	@RequestMapping(method=RequestMethod.POST,value="/user/removeCard")
	private void removeCard(@RequestParam("userId") String userId, @RequestParam("cardId") String cardId) {
		userService.removeCard(Integer.parseInt(userId),Integer.parseInt(cardId));
	}

	@RequestMapping(method=RequestMethod.GET,value="/user/{id}/account/canBuy/{price}")
	public boolean canBuy(@PathVariable String id, @PathVariable String price) {
		return userService.canBuy(id, price);
	}

	@RequestMapping(method=RequestMethod.GET,value="/user/connect/{login}")
	public boolean connectUser(@PathVariable String login, @RequestBody String password) {
		return userService.connectUser(login, password);
	}

}

package com.example.common.Client;





import com.example.common.DTO.CardDTO;
import com.example.common.DTO.UserDTO;
import feign.QueryMap;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name="card",url="http://localhost:8091"/*,configuration = {FeignInterceptor.class}*/)
public interface CardClient {

    @RequestMapping(method = RequestMethod.GET, value = "/cards/{cardId}")
    CardDTO getcard(@PathVariable("cardId") int cardId);

    @RequestMapping(method=RequestMethod.PUT,value="/card/{id}")
    void updateCard(@RequestBody CardDTO card,@PathVariable String id);

    @RequestMapping(method = RequestMethod.GET, value = "/cards")
    List<CardDTO> getAllCards();

    @RequestMapping(method = RequestMethod.GET, value = "/card/rand/{nbr}/{userId}")
    List<CardDTO> getRandCard(@PathVariable("userId") Integer userId, @PathVariable("nbr") int nbr);

    @RequestMapping(method = RequestMethod.GET, value = "/card/player/{playerId}")
    List<CardDTO> getCardsByPlayerId(@PathVariable("playerId") Long playerId);

    @RequestMapping(method = RequestMethod.POST, value = "/card/generate",consumes = "application/json")
    void generateCard(Long playerId);


}

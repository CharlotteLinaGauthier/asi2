package com.example.common.Client;


import com.example.common.DTO.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient(name="user",url= "http://localhost:8090"/*,configuration = {FeignInterceptor.class}*/)
public interface UserClient {


    @RequestMapping(value = "/players/sell", method = RequestMethod.POST)
    void sellCard(@RequestParam int playerId, @RequestParam float price);

    @RequestMapping(value = "/players/buy", method = RequestMethod.POST)
    void buyCard(@RequestParam int playerId,@RequestParam float price);

    @RequestMapping(method = RequestMethod.GET, value = "/players")
    List<UserDTO> getCards();

    @RequestMapping(method = RequestMethod.GET, value = "/players/{playerId}")
    UserDTO getPlayerById(@PathVariable("playerId") int playerId);

    @RequestMapping(method = RequestMethod.GET, value = "/players/playerName/{playerName}")
    UserDTO getPlayerByUsername(@PathVariable("playerName") String playerName);

    @RequestMapping(method = RequestMethod.POST, value = "/players")
    UserDTO register(@RequestBody UserDTO player);

    @RequestMapping(method = RequestMethod.GET, value="/user/{id}/account/canBuy/{price}")
    boolean canBuy(@PathVariable("id") int id, @PathVariable("price") float price);

    @RequestMapping(method = RequestMethod.GET, value="/user/connect/{login}")
    boolean connectUser(@PathVariable("login") String login, @RequestBody String password);

    @RequestMapping(method = RequestMethod.POST, value="/user/removeCard")
    void removeCard(@RequestParam("userId") String userId, @RequestParam("cardId") String cardId);

    @RequestMapping(method = RequestMethod.POST, value="/user/addCard")
    void addCard(@RequestParam("userId") String userId, @RequestParam("cardId") String cardId);

}

package com.example.common.DTO;

import java.util.HashSet;
import java.util.Set;

public class StoreDTO {

    private Integer id;
    private String name;

    private Set<CardDTO> cardList = new HashSet<>();

    public StoreDTO(Integer id, String name, Set<CardDTO> cardList){
        this.id = id;
        this.name = name;
        this.cardList = cardList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CardDTO> getCardList() {
        return cardList;
    }

    public void setCardList(Set<CardDTO> cardList) {
        this.cardList = cardList;
    }

}

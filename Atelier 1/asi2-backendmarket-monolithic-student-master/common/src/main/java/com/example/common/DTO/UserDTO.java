package com.example.common.DTO;


import com.example.common.Client.UserClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.Set;



public class UserDTO {
	private Integer id;
	private String login;
	private String pwd;
	private float account;
    private Set<Integer> cardList = new HashSet<>();

    public UserDTO(Integer id, String login, String pwd,Set<Integer> cardList,float account) {
    	this.id=id;
    	this.login=login;
    	this.pwd=pwd;
    	this.cardList=cardList;
    	this.account=account;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Set<Integer> getCardList() {
		return cardList;
	}

	public void setCardList(Set<Integer> cardList) {
		this.cardList = cardList;
	}

	public boolean canBuy(float price){
		return false;
	}

	public void addCard(Integer d) {
    	cardList.add(d);
	}

	public float getAccount() {
		return account;
	}

	public void setAccount(float account) {
		this.account = account;
	}
}

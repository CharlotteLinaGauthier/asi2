package com.example.auth.Controller;

import com.example.auth.Service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class AuthRestController {



    @Autowired
    private AuthService authService;


    @RequestMapping(method= RequestMethod.POST,value="/connect/{login}")
    private boolean askConnectUser(@PathVariable("login") String login, @RequestBody String password) {
        return authService.askConnectUser(login,password);
    }
}

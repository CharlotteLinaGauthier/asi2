package com.example.auth.Service;


import com.example.common.Client.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    UserClient userService;


    public boolean askConnectUser(String login, String password) {
        return userService.connectUser(login, password);

    }
}

package com.example.card.Repository;


import com.example.card.model.CardReference;
import org.springframework.data.repository.CrudRepository;

public interface CardRefRepository extends CrudRepository<CardReference, Integer> {

}

package com.example.card.Repository;




import com.example.card.model.CardModel;
import com.example.common.DTO.UserDTO;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    List<CardModel> findByUserId(Integer u);
}

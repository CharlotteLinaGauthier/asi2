package com.example.card.Controller;


import com.example.card.Service.CardModelService;
import com.example.card.model.CardModel;
import com.example.common.DTO.CardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CardRestController {

	

	@Autowired
	private CardModelService cardModelService;
	
	@RequestMapping("/cards")
	private List<CardDTO> getAllCards() {
		List<CardDTO> cLightList=new ArrayList<>();
		for(CardModel c:cardModelService.getAllCardModel()){
			cLightList.add(new CardDTO(c.getId(),c.getEnergy(),c.getHp(),c.getDefence(),c.getAttack(),c.getPrice(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(), c.getSmallImgUrl(), c.getUser()));
		}
		return cLightList;

	}
	
	@RequestMapping("/card/{id}")
	private CardDTO getCard(@PathVariable String id) {
		Optional<CardModel> rcard;
		rcard= cardModelService.getCard(Integer.valueOf(id));
		if(rcard.isPresent()) {
			CardModel c = rcard.get();
			return new CardDTO(c.getId(),c.getEnergy(),c.getHp(),c.getDefence(),c.getAttack(),c.getPrice(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(), c.getSmallImgUrl(), c.getUser());
		}
		return null;

	}
	
	@RequestMapping(method= RequestMethod.POST,value="/card")
	public void addCard(@RequestBody CardModel card) {
		cardModelService.addCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/card/{id}")
	public void updateCard(@RequestBody CardDTO card,@PathVariable String id) {
		CardModel c = new CardModel(card.getName(),card.getDescription(),card.getFamily(),card.getAffinity(),card.getEnergy(),card.getHp(),card.getDefence(),card.getAttack(),card.getImgUrl(),card.getSmallImgUrl(),card.getPrice() );
		c.setId(card.getId());
		c.setUser(Integer.parseInt(id));
		cardModelService.updateCard(c);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/card/rand/{nbr}/{userId}")
	public List<CardDTO> getRandCard(@PathVariable("userId") Integer userId, @PathVariable("nbr") int nbr){
		 List<CardModel> cardModels = cardModelService.getRandCard(nbr,userId);
		List<CardDTO> cardDTOS = new ArrayList<>();
		for (CardModel c: cardModels) {
			cardDTOS.add(new CardDTO(c.getId(),c.getEnergy(),c.getHp(),c.getDefence(),c.getAttack(),c.getPrice(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(), c.getSmallImgUrl(), c.getUser()));
		}
		return cardDTOS;
	}

	@RequestMapping(method=RequestMethod.DELETE,value="/card/{id}")
	public void deleteUser(@PathVariable String id) {
		cardModelService.deleteCardModel(Integer.valueOf(id));
	}

	@RequestMapping("/cards_to_sell")
	private List<CardDTO> getCardsToSell() {
		List<CardDTO> list=new ArrayList<>();
		for( CardModel c : cardModelService.getAllCardToSell()){
			CardDTO cLight=new CardDTO(c.getId(),c.getEnergy(),c.getHp(),c.getDefence(),c.getAttack(),c.getPrice(), c.getName(), c.getDescription(), c.getFamily(), c.getAffinity(), c.getImgUrl(), c.getSmallImgUrl(), c.getUser());
			list.add(cLight);
		}
		return list;

	}
	
}

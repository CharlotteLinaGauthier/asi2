package com.example.store.Model;

import com.example.common.DTO.CardDTO;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
public class StoreModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	
@	ElementCollection
    private Set<Integer> cardList = new HashSet<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Integer> getCardList() {
		return cardList;
	}

	public void setCardList(Set<Integer> cardList) {
		this.cardList = cardList;
	}

	public void addCard(Integer card) {
		//card.setStore(this); faire un appel pour remplacer ca
		this.cardList.add(card);
	}
	

}

package com.example.store.Repository;

import com.example.store.Model.StoreModel;
import org.springframework.data.repository.CrudRepository;



public interface StoreRepository extends CrudRepository<StoreModel, Integer> {
	

}

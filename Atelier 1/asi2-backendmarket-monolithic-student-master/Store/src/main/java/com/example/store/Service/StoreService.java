package com.example.store.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.example.common.Client.CardClient;
import com.example.common.Client.UserClient;
import com.example.common.DTO.CardDTO;
import com.example.common.DTO.UserDTO;
import com.example.store.Model.StoreModel;
import com.example.store.Repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class StoreService {

	@Autowired
	CardClient cardService;
	@Autowired
	UserClient userService;
	@Autowired
	StoreRepository storeRepository;
	
	private StoreModel store;
	
	public void generateCards( int nb) {
		StoreModel store =new StoreModel();
		
		List<CardDTO> cardList=cardService.getRandCard(0,nb);

	}
	
	public boolean buyCard(int user_id, int card_id) {
		UserDTO u=userService.getPlayerById(user_id);
		CardDTO c=cardService.getcard(card_id);
		if((u == null) || (c==null)){
			return false;
		}

		if(userService.canBuy(u.getId(), c.getPrice())) {
			//u.addCard(c.getId());
			//u.setAccount(u.getAccount()-c.getPrice());
			userService.addCard(u.getId().toString(),c.getId().toString());
			cardService.updateCard(c,String.valueOf(user_id));
			return true;
		}else {
			System.out.println(" YAHOOooOooooooo  HELOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");

			return false;
		}
	}
	
	public boolean sellCard(Integer user_id, Integer card_id) {
		UserDTO u=userService.getPlayerById(user_id);
		CardDTO c=cardService.getcard(card_id);
		if(u == null || c == null){
			return false;
		}
		//c.setStore(this.store);
		cardService.updateCard(c,"0");
		userService.removeCard(u.getId().toString(),c.getId().toString());
		return true;
	}
	
	public Set<Integer> getAllStoreCard(){
		return this.store.getCardList();
	}
}

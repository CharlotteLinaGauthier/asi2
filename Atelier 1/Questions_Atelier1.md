### QUESTIONS ATELIER 1

---
TODO :  Vérifications et compléments par chacun 

- [ ] Charlotte
- [ ] Dorian
- [ ] Hugo
- [ ] Matthieu
- [X] Béa

**1. Qu’est ce que le CROSS ORIGIN ? En quoi cela est-il dangereux ?**

Le «  Cross-origin resource sharing » (CORS) ou « partage des ressources entre origines multiples » (en français, moins usité) est un mécanisme qui consiste à ajouter des en-têtes HTTP afin de permettre à un agent utilisateur d'accéder à des ressources d'un serveur situé sur une autre origine que le site courant. Un agent utilisateur réalise une requête HTTP multi-origine (cross-origin) lorsqu'il demande une ressource provenant d'un domaine, d'un protocole ou d'un port différent de ceux utilisés pour la page courante.  

Pour des raisons de sécurité, les requêtes HTTP multi-origine émises depuis les scripts sont restreintes. Ainsi, XMLHttpRequest et l'API Fetch respectent la règle d'origine unique. Cela signifie qu'une application web qui utilise ces API peut uniquement émettre des requêtes vers la même origine que celle à partir de laquelle l'application a été chargée, sauf si des en-têtes CORS sont utilisés.
[source](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS)

Le risque inhérent à CORS est donc le partage non controllé des données. Une mauvaise configuration peut entrainer des fuites de données utilisateurs, de clés API ou autres. Une mauvaise configuration peut être exploitées de différentes façons:
- Origin reflection : Quand un serveur génère un Access-Control-Allow-Origin à partir de l'origine reçue en requete, n'importe quelle site pourra eccéder à ses ressources.
- Null Origin : N'importe quel site peut y  accéder en utilisant une sandbox
- Pre Domain WildCard :  Si un domain non enregistré a été autorisé, un hacker pourra enregistrer le dit domaine et voler les données
- Post-Domain WildCard : Création d'un domaine suivant le même patternn que des domaines autorisés
- SUb-Domain allowed : Si le sous-domaine présente une vulnérabilité, le hacker pourra l'exploiter pour remonter à la source

[source](https://medium.com/@ehayushpathak/security-risks-of-cors-e3f4a25c04d7) 


**2. Comment REACTJS fait-il pour afficher rapidement les modifications sur les composants ?**

React DOM compare l’élément et ses enfants avec la version précédente, et applique uniquement les mises à jour DOM nécessaires pour refléter l’état voulu.
[source](https://fr.reactjs.org/docs/rendering-elements.html)


**3. Quelle est la fonction essentielle de REACTJS ?**

Le but principal de REACT est de faciliter la création d'application web monopage, via la création de composants dépendant d'un état et générant une page (ou portion) HTML à chaque changement d'état.
[source](https://fr.wikipedia.org/wiki/React)

**4. Quelle est la fonction essentielle de FLUX ?**

Flux est une architecture pour application web que Facebook a proposée pour construire des applications avec React et permet de répondre auxx besoins suivants :

- éviter la dette technique / possibilité d’évolution aisée
- réduire le nombre de dépendances entre les différents Services/Controllers de l’application
- empêcher les effets de bords indésirables
- savoir précisément quel est l’état de la donnée à tout instant
- gérer une grande quantité de données et de changements sur celle-ci
- augmenter la possibilité de réutiliser du code
- performances

Voici donc quelques solutions clés pour y répondre :

- immutabilité
- flux unidirectionnel de données
- conteneurs de stockage isolés pour les données
- librairies découpées/légères

Ces points clés sont la base de l’architecture Flux et la philosophie de l’univers React en général.

Le point essentiel de FLUX est que cette architecture implemente un flux unidirectionnel des données.

![archi Flux](https://blog.engineering.publicissapient.fr/wp-content/uploads/2015/02/Flux.png)


[source](https://blog.link-value.fr/react-et-architecture-flux-d763c5e0ecf8)
[autre source](https://putaindecode.io/articles/flux-qu-est-ce-que-c-est/)
[source image](https://blog.engineering.publicissapient.fr/2015/02/11/choisir-une-architecture-flux-pour-son-projet-react/)

**5. Qu’est ce que REDUX ?**

Redux est une bibliothèque compacte qui fournit un conteneur d’état « predictable state container » pour les applications JavaScript. Redux est développé par Dan Abramov depuis mai 2015.

Redux est une librairie très légère, et il n’y a que 3 concepts primordiaux à intégrer .
Global state : Avec Redux, tout l’état d’une application est décrit comme un objet.
Actions : Pour modifier l’état de l’application, il faut utiliser une action. Les actions sont des objects, de simples dénominations servant à représenter une interaction. Elles n'apportent rien de fonctionnel, et ne sont là que pour apporter un nom ("type") qui les représente et, optionnellement, un ou plusieurs arguments.
Reducers: Ensuite, pour relier les state du global state défini précédemment aux actions, on écrit ce que l’on appelle des reducers ce sont des fonctions pures qui prennent en arguments un state et une action, et qui retournent le state après que l’ action ait été effectuée.
Tout cela permet de déplacer la logique metier dans le reducer et de mieux gérer les dépendances hierarchiques. Cela permet notamment de metrte des composants à jour directement lorsqu'une propriété est modifiées plutôt que passer par l'entiereté des objets 
[source](https://www.softfluent.fr/blog/utiliser-redux-avec-react-js/)
[autre source](https://medium.com/@aymenzaouali/pourquoi-utiliser-redux-f38b55430d13)


**6. Qu’est ce que JMS ? Est-ce spécifique à Springboot ?**

L'interface de programmation Java Message Service (JMS) permet d'envoyer et de recevoir des messages de manière asynchrone entre applications ou composants Java. JMS permet d'implémenter une architecture de type MOM (message oriented middleware). Un client peut également recevoir des messages de façon synchrone dans le mode de communication point à point.
L'API JMS permet aux applications Java de s'interfacer avec des intergiciels (middleware) à messages ou MOM. Les MOM permettent des interactions entre composants applicatifs dans un cadre faiblement couplé, asynchrone et fiable.
[source](https://fr.wikipedia.org/wiki/Java_Message_Service)
Pas spécifique à SpringBoot source= Moi donc pas sûr.

**7. Quelles sont les différents modes de transmissions de JMS ?**

1. Topics : In JMS a Topic implements publish and subscribe semantics. When you publish a message it goes to all 
the subscribers who are interested - so zero to many subscribers will receive a copy of the message. 
Only subscribers who had an active subscription at the time the broker receives the message will get a 
copy of the message

2. Queues : A JMS Queue implements load balancer semantics. A single message will be received by exactly one 
consumer. If there are no consumers available at the time the message is sent it will be kept until a 
consumer is available that can process the message. If a consumer receives a message and does not 
acknowledge it before closing then the message will be redelivered to another consumer. A queue can 
have many consumers with messages load balanced across the available consumers.

source : le cours

**8. Quel est le mode de transmission activé par défaut dans activeMq ?**

 TCP mais alors pas sûre du tout

**9. Qu’est ce que activeMq ?**

Il s'agit d'un message broker, c'est à dire un module logiciel qui peut valider, transformer et rediriger les messages1. Il agit comme médiateur entre les émetteurs et les récepteurs en leur permettant de communiquer efficacement avec un couplage minimum entre eux.
[source](https://fr.wikipedia.org/wiki/Agent_de_messages)

**10. Quels avantages proposent un bus de communication vis-à-vis de requêtes http classiques ?**

HTTP est un protocole synchrone, ce qui veut dire que le client ne peut continuer sa tâche que lorsqu'il a reçu la réponse du serveur HTTP. Il y a donc forcement un temps d'arrêt dans l'exécution du client, un temps d'attente.
L'avantage d'un bus de communication réside dans son exéccution asynchrone. Le traitement du côté du client peut continuer à s'exécuter.
[source](https://docs.microsoft.com/fr-fr/dotnet/architecture/microservices/architect-microservice-container-applications/communication-in-microservice-architecture) 

**11. Comment faire pour partager des modules Maven avec un partenaire extérieur ?**

Créer des librairies?? JE C PA

**12. Comment faire pour exporter un composant REACTJS ?**

Il existe 2 type d'export :

1. Named Export
```JS
class TodoList extends React.Component {
  render() {
    return (
      <div>
        <h1>Todo list</h1>
      </div>
    );
  }
}
export { TodoList };
```

2. Default Export
```JS
class TodoList extends React.Component {
  render() {
    return (
      <div>
        <h1>Todo list</h1>
      </div>
    );
  }
}

export default TodoList;
```
En général, la bonne pratique defini l'export default comme méthode à utiliser.
[source](https://dev.to/petr7555/export-import-components-in-react-ea8)

**13. Quel est le pré-requis pour Springboot afin de pouvoir convertir automatiquement le message reçu dans un bus de communication en objet ?**

Il faut utiliser le package org.springframework.jms.support.converter qui fournit la classe MessageCoverter permettant la conversion d'un message JMS en object JAva. 
[source](https://www.jmdoudoux.fr/java/dej/chap-spring_JMS.htm)

**14. Comment est réalisée la traduction des messages reçus (bus de communication ou request http) en objet Java ? Quelles sont les prérequis ? Est-ce toujours possible ?**
Chaque classe/objet Java devra posséder les deux méthodes suivantes :
1. toMessage()  : envoi l'objet au format JSON string
2. fromMessage() : converti un JSON en objet (ObjectMapper)

La classe devra aussi conporter l'annotation @Component et implementer MessageConverter.
[source](https://codenotfound.com/spring-jms-message-converter-example.html)

[Autre méthode](https://www.baeldung.com/spring-httpmessageconverter-rest)

**15. Quelles sont les fonctionnalités des différentes annotations en Springboot ?:**
- @EnableJpaRepositories : Annotation to enable JPA repositories. Will scan the package of the annotated configuration class for Spring Data repositories by default.
[source](https://docs.spring.io/spring-data/data-jpa/docs/current/api/org/springframework/data/jpa/repository/config/EnableJpaRepositories.html)  


- @EntityScan : Configures the base packages used by auto-configuration when scanning for entity classes.
Using @EntityScan will cause auto-configuration- 
  - Set the packages scanned for JPA entities.
  - Set the initial entity set used with Spring Data MongoDB, Neo4j, Cassandra and Couchbase mapping contexts.
[source](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/domain/EntityScan.html)  


- @ComponentScan : Configures component scanning directives for use with @Configuration classes. Provides support parallel with Spring XML's <context:component-scan> element. 
[source](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/context/annotation/ComponentScan.html)  

*"Bonus" :  différence entre @EntityScan et @ComponentScan :*
@EntityScan should specify which packages do we want to scan for entity classes. On the other hand, @ComponentScan is a choice when specifying which packages should be scanned for Spring beans.
[source](https://www.baeldung.com/spring-entityscan-vs-componentscan)
|| ActiveMQ | RabbitMQ | ZeroMQ
|--------------|-------| ----------- | ----------- |
| Langage | JAVA | Erlang | C++|
| Cross-Platform | Oui | Oui | Oui|
| Open Source | Oui | Oui | Oui|
| Langage multiple | Oui | Oui | Oui|
| Protocoles | AMQP, AUTO, MQTT, OpenWire, REST, RSS and Atom, Stomp, WSIF, WS Notification, XMPP, WebSocket | AMQP, STOMP, MQTT, HTTP | TCP, inproc, PGM, IPC, TIPC, NORM, SOCKS5, UDP (multicast et unicast)|
| Administration| S'il ya un broker, l'administration est possible via une Web Console | Obligatoire | zero|
| Coût | Gratuit | Gratuit mais il existe une license commerciale | Gratuit|
| Synchrone/Asynchrone | Synchrone par défaut, mais possible de faire de l'asynchone | Les deux | Asynchrone|
| Message patterns | Message Queue, Pub-Sub | Message Queue, PUB-SUB, ROUTING, RPC | Req and Rep, Pub-Sub, Req and Router, Dealer and Rep, Dealer and Router, Dealer and Dealer, Router and Router, Push and Pull, Pair and Pair|
| Tutoriels | Attune, NobleProg, TytoEASE, Tomitribe, Savoir Technologies | LearnQuest, Opensource architect, Erlang Solutions, et pivotal Software |Pieter Hintjens workshops|
| Documentation | http://activemq.apache.org/getting-started.html| https://www.rabbitmq.com/documentation.html | http://zguide.zeromq.org/page:all |


**ActiveMQ**
- Peut être déployé avec les deux courtier et P2P topologies
- Facile de mettre en œuvre les scénarios avancés mais généralement au détriment de la performance brute.

**RabbitMQ**
- Leader pour le protocole AMQP
- Possède une broker architecture (messages en files d'attentes sur le noued cenral)
- Facile à utiliser et à déployer 
- Moins évolutif
- Plus lent (noeud central provoque de la latence)

**ZeroMQ**
- Système très léger de messagerie
- Spécialement conçu pour le haut débit
- Faible latence
- A jour sur les avancées 
- Flexible
- Compliqué à prendre en main
- Beaucoup de configuration necessaire par l'utilisateur

Tout trois ont des APIs clients pour presque tous les langages, ont une documentation fournie et une communauté active.

